import React from 'react';
import './style.css'
class TaskInput extends React.Component {
    state = {
        title: ''
    }
    changeHandler = (event) => {
        this.setState({
            title: event.target.value
        })

    }
    submitHandler = (event) => {
        event.preventDefault();
        this.props.addItem(this.state.title)
        this.setState({
            title: ''
        })
    }

    render() {
        return (
            <div className="input-group">
                <form onSubmit={this.submitHandler}>
                    <input type="text"
                        className="inp"
                        placeholder="Input here...."
                        value={this.state.title}
                        onChange={this.changeHandler}
                    ></input>
                    <button type="submit" className="btn">ADD</button>
                </form>
            </div>
        )
    }
}
export default TaskInput 