import React from 'react';
import TaskInput from '../TaskInput'
import TaskView from '../TaskView'
import TaskItem from '../TaskItem'
import './style.css'
import ImageTask from '../ImageTask'

class TodoApp extends React.Component {
    state = {
        todo: [

        ],
        checkAll: true
    }
    addItemHandler = (inputItem) => {
        const newTask = {
            id: Date.now(),
            title: inputItem,
            done: false
        }
        const newTodo = [...this.state.todo, newTask];
        this.setState({
            todo: newTodo
        })
    }
    deleteHandler = (event, id) => {
        event.stopPropagation();
        const nowTodo = this.state.todo;
        const newTodo = nowTodo.filter(todo => {
            return todo.id !== id
        })
        // console.log(newTodo);
        this.setState({
            todo: newTodo
        })

    }
    checkDoneHandler = (id) => {
        const nowTodo = this.state.todo;
        const indexOfTask = nowTodo.findIndex((todo) => {
            return todo.id === id
        })
        const nowStatus = nowTodo[indexOfTask].done;
        nowTodo[indexOfTask].done = !nowStatus;

        this.setState({
            todo: nowTodo,
        })

    }
    sortHandler = () => {
        const nowTD = this.state.todo;
        nowTD.sort((todoA, todoB) => {
            return todoA.done - todoB.done;
        })
        this.setState({
            todo: nowTD
        })
        // alert('sorted')
    }
    checkAllHandler = () => {
        const checkAllNow = this.state.checkAll;
        const newTD = this.state.todo;
        console.log(checkAllNow);
        if (this.state.checkAll === true) {
            for (let td of newTD) {
                td.done = true;
            }
        } else {
            for (let td of newTD) {
                td.done = false;
            }
        }
        this.setState({
            todo: newTD,
            checkAll: !checkAllNow
        })
    }
    countDone = () => {
        let count = 0;
        for (let td of this.state.todo) {
            if (td.done === true) {
                count++;
            }
        }
        return count;
    }
    updateHandler = (id, newTitle) => {
        const nowTD = this.state.todo;
        const indexOfTask = nowTD.findIndex(todo => {
            return todo.id === id
        })
        nowTD[indexOfTask].title = newTitle;
        this.setState({
            todo: nowTD
        })
    }
    deleteCheckedHandler = () => {
        // if()
        const newTD = this.state.todo
        const td = newTD.filter(todo => {
            return todo.done === false;
        })
        this.setState({
            todo: td,
            checkAll: !this.state.checkAll
        })
        console.log(this.state.checkAll)
    }
    labelCheckAll = () => {
        let count = 0;
        let label = '';
        for (let todo of this.state.todo) {
            if (todo.done === true) { count++; }
        }
        if (count === this.state.todo.length) {
            label += 'Uncheck All';
            this.state.checkAll = false;
        } else {
            label += 'Check All';
            this.state.checkAll = true;
        }
        return label;

    }
    render() {
        return (
            <div>
                <h1>Todo List make by Me</h1>
                <TaskInput addItem={this.addItemHandler} />
                <TaskView>

                    <h3 className="checked">Checked:{this.countDone()}/{this.state.todo.length}</h3>

                    {this.state.todo.length > 0 ?
                        this.state.todo.map((item, index) => {
                            return <TaskItem
                                stt={index + 1}
                                id={item.id}
                                title={item.title}
                                done={item.done}
                                key={item.id}
                                delete={(event) => this.deleteHandler(event, item.id)}
                                checkDone={() => this.checkDoneHandler(item.id)}
                                update={this.updateHandler}
                            ></TaskItem>

                        })
                        :
                        <ImageTask />
                    }
                </TaskView>
                {this.state.todo.length > 0 ? <div className="btn-group">
                    <div className="btn-gr-left">
                        <button className="btn-sort"
                            onClick={this.sortHandler}
                        >Sort</button>
                        <button className="btn-checkall"
                            onClick={this.checkAllHandler}
                        >{this.labelCheckAll()}</button>
                    </div>
                    <div className="btn-gr-right">
                        <button className="btn-delete-checked"
                            onClick={this.deleteCheckedHandler}
                        >Delete Checked</button>
                    </div>
                </div> : null}
            </div>
        )
    }
}
export default TodoApp; 