import React from 'react';
import './style.css'
class TaskItem extends React.Component {
    state = {
        titlePopup: this.props.title,
        showPopup: false
    }
    togglePopup = (event) => {
        event.stopPropagation();
        this.setState({
            showPopup: !this.state.showPopup
        })
    }
    changeTitle = (event) => {
        this.setState({
            titlePopup: event.target.value
        })
    }
    updateNewTitle = (event) => {
        this.props.update(this.props.id, this.state.titlePopup);
        this.togglePopup(event);

    }
    render() {
        return (

            <div className="item" >
                <p className={`${this.props.done ? "done" : ""}`} onClick={this.props.checkDone}>
                    {this.props.stt}. {this.props.title}
                </p>
                <div className="btn-gr">
                    <button className="btn-edit" onClick={this.togglePopup}>Edit</button>
                    <button className="btn-del" onClick={this.props.delete}>Delete</button>
                </div>
                {this.state.showPopup ?
                    <div className="popup">
                        <div className="popup-inner">
                            <div className="popup-close">
                                <span className="close" onClick={this.togglePopup}>X</span>
                            </div>
                            <h1>Edit Task</h1>
                            <label>Title</label>
                            <input type="text" className="ip-pop" value={this.state.titlePopup} onChange={this.changeTitle}></input>
                            <div className="btn-gr-popup">
                                <button className="btn-pop" type="button" onClick={this.updateNewTitle}>Save Change</button>
                            </div>
                        </div>
                    </div> : null}
            </div>

        )
    }
}
export default TaskItem