import React from 'react';
import './style.css'
const TaskView = (props) => {
    return (
        <div className="task-view">
            {props.children}

        </div>
    )
}
export default TaskView;