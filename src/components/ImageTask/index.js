import React from 'react';
import freedom from '../../assets/images/123.jpg'
import NoTask from '../NoTask'
import './style.css'
const ImageTask = () => {
    return (
        <div>
            <img src={freedom} className="img" />
            <NoTask />
        </div>
    )
}
export default ImageTask;